#![cfg_attr(feature = "external_doc", feature(external_doc))]
#![cfg_attr(feature = "external_doc", doc(include = "../README.md"))]
#![cfg_attr(
    not(feature = "external_doc"),
    doc = "Build with `cargo doc +nightly --features external_doc` for crate-level documentation.",
)]
#![warn(missing_docs)]
